package com.fribees.api;


import com.fribees.business.RideService;
import com.fribees.business.dto.AbsoluteEventsDTO;
import com.fribees.business.dto.Granularity;
import com.fribees.business.dto.StatisticsDTO;
import com.fribees.business.dto.UserPerformanceDTO;
import com.fribees.persistence.Ride;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/rides")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Tag(name = "Rides", description = "Ride statistics and ride related queries")
public class RideResource {

    private final RideService rideService;

    @Inject
    public RideResource(RideService rideService) {
        this.rideService = rideService;
    }


    @GET
    @Path("{userId}")
    public List<Ride> getRidesForUser(
            @Parameter(
                    description = "The requesting user id",
                    required = true,
                    example = "153",
                    schema = @Schema(type = SchemaType.STRING))
            @PathParam("userId") long userId) {
        return rideService.getAllForUser(userId);
    }

    @GET
    @Path("{userId}/statistics")
    @Operation(summary = "Get ride statistics", description = "Get ride statistics. Statistics can be calculated in different time frames, using the granularity parameter.")
    public List<StatisticsDTO> getStatistics(
            @Parameter(
                    description = "The requesting user id",
                    required = true,
                    example = "153",
                    schema = @Schema(type = SchemaType.STRING))
            @PathParam("userId") long userId, @QueryParam("granularity") Granularity granularity) {
        return rideService.getRideStatistics(userId, granularity);
    }

    @GET
    @Path("{userId}/statistics/five-rides")
    @Operation(summary = "Get statistics of last five rides")
    public AbsoluteEventsDTO getFiveRideStatistics(
            @Parameter(
                    description = "The requesting user id",
                    required = true,
                    example = "153",
                    schema = @Schema(type = SchemaType.STRING))
            @PathParam("userId") long userId) {
        return rideService.getLastFiveRidesStatistics(userId);
    }

    @GET
    @Path("{userId}/statistics/performance")
    public UserPerformanceDTO getUserPerformance(
            @Parameter(
                    description = "The requesting user id",
                    required = true,
                    example = "153",
                    schema = @Schema(type = SchemaType.STRING))
            @PathParam("userId") long userId) {
        return rideService.getUserPerformance(userId);
    }

    @GET
    @Path("mock")
    public Response mock() {
        rideService.saveMock();
        return Response.ok().build();
    }
}
