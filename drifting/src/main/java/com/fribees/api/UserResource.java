package com.fribees.api;


import com.fribees.business.UserService;
import com.fribees.business.dto.AuthDTO;
import com.fribees.persistence.AppUser;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/users")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Tag(name = "Users", description = "User authentication")
public class UserResource {

    private final UserService userService;

    @Inject
    public UserResource(UserService userService) {
        this.userService = userService;
    }

    @Operation(summary = "Authenticate the user", description = "Authenticate the user using plain text username and password. Subject to change.")
    @POST
    @Path("authenticate")
    public AppUser authenticate(AuthDTO authDTO) {
        return userService.authenticate(authDTO.getUsername(), authDTO.getPassword());
    }

    @Operation(summary = "Get a user by id")
    @GET
    @Path("{userId}")
    public AppUser getUserById(
            @Parameter(
                    description = "The requesting user id",
                    required = true,
                    example = "153",
                    schema = @Schema(type = SchemaType.STRING))
            @PathParam("userId") long userId) {
        return userService.getUserById(userId);
    }

    @Operation(summary = "Get a user by id")
    @POST
    public void saveUsers(List<AppUser> appUsers) {
        userService.saveUsers(appUsers);
    }
}
