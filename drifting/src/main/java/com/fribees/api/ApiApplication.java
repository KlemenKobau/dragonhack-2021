package com.fribees.api;

import org.eclipse.microprofile.openapi.annotations.OpenAPIDefinition;
import org.eclipse.microprofile.openapi.annotations.info.Info;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@OpenAPIDefinition(
        info = @Info(
                title = "Road rage API",
                version = "1.0.0")
)
@ApplicationPath("/v1")
public class ApiApplication extends Application {
}
