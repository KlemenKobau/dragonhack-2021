package com.fribees.api;

import com.fribees.business.AccEventService;
import com.fribees.business.dto.ArduinoDTO;
import com.fribees.persistence.AccelerationEvent;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/accelerations")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Tag(name = "Accelerations", description = "Methods related to acceleration events")
public class AccelerationResource {

    private final AccEventService accEventService;

    @Inject
    public AccelerationResource(AccEventService accEventService) {
        this.accEventService = accEventService;
    }

    @POST
    @Operation(summary="Save a list of accelerations")
    public void saveAccelerations(List<AccelerationEvent> accelerationEvents) {
        accEventService.saveEvents(accelerationEvents);
    }

    @POST
    @Path("arduino")
    @Operation(summary="Save accelerations from arduino")
    public void saveArduinoAcceleration(List<ArduinoDTO> arduinoDTOS) {
        accEventService.saveArduinoEvents(arduinoDTOS);
    }
}
