package com.fribees.business.dto;

import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Schema(name = "Absolute events", description="Wrapper class for the absolute number of events")
public class AbsoluteEventsDTO {

    @Schema(description="Number of events that happened")
    private long numberOfEvents;

    public long getNumberOfEvents() {
        return numberOfEvents;
    }

    public void setNumberOfEvents(long numberOfEvents) {
        this.numberOfEvents = numberOfEvents;
    }
}
