package com.fribees.business.dto;

import org.eclipse.microprofile.openapi.annotations.media.Schema;

import java.time.Instant;

@Schema(name = "Statistics", description="POJO containing the calculated statistics")
public class StatisticsDTO {

    @Schema(description="When the first ride included in this statistic started")
    private Instant startTime;

    @Schema(description="When the last ride included in this statistic started")
    private Instant endTime;

    @Schema(description="Average number of moments in a ride")
    private double avgMomentsPerRide;

    @Schema(description="Average number of moments per hour")
    private double avgMomentsPerHour;

    public Instant getStartTime() {
        return startTime;
    }

    public void setStartTime(Instant startTime) {
        this.startTime = startTime;
    }

    public Instant getEndTime() {
        return endTime;
    }

    public void setEndTime(Instant endTime) {
        this.endTime = endTime;
    }

    public double getAvgMomentsPerRide() {
        return avgMomentsPerRide;
    }

    public void setAvgMomentsPerRide(double avgMomentsPerRide) {
        this.avgMomentsPerRide = avgMomentsPerRide;
    }

    public double getAvgMomentsPerHour() {
        return avgMomentsPerHour;
    }

    public void setAvgMomentsPerHour(double avgMomentsPerHour) {
        this.avgMomentsPerHour = avgMomentsPerHour;
    }
}
