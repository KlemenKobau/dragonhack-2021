package com.fribees.business.dto;

import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Schema(description="Describes the granularity in use with the statistics")
public enum Granularity {
    DAYS,
    MONTHS,
    ALL_TIME
}
