package com.fribees.business.dto;

import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Schema(name="Arduino event", description="Arduino sends special POJOs that are then transformed by the backend")
public class ArduinoDTO {

    @Schema(description="User's id")
    private Long userId;

    @Schema(description="Time since the car startup")
    private Long amountPassedInSeconds;

    @Schema(description="Drive duration")
    private Long driveDuration;

    @Schema(description="Arduino's internal drive id")
    private Long driveId;

    @Schema(description="Acceleration vector's size")
    private Double size;

    @Schema(description="Acceleration vector's angle")
    private Double angle;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getAmountPassedInSeconds() {
        return amountPassedInSeconds;
    }

    public void setAmountPassedInSeconds(Long amountPassedInSeconds) {
        this.amountPassedInSeconds = amountPassedInSeconds;
    }

    public Long getDriveDuration() {
        return driveDuration;
    }

    public void setDriveDuration(Long driveDuration) {
        this.driveDuration = driveDuration;
    }

    public Long getDriveId() {
        return driveId;
    }

    public void setDriveId(Long driveId) {
        this.driveId = driveId;
    }

    public Double getSize() {
        return size;
    }

    public void setSize(Double size) {
        this.size = size;
    }

    public Double getAngle() {
        return angle;
    }

    public void setAngle(Double angle) {
        this.angle = angle;
    }

    @Override
    public String toString() {
        return "ArduinoDTO{" +
                "userId=" + userId +
                ", amountPassedInSeconds=" + amountPassedInSeconds +
                ", driveDuration=" + driveDuration +
                ", driveId=" + driveId +
                ", size=" + size +
                ", angle=" + angle +
                '}';
    }
}
