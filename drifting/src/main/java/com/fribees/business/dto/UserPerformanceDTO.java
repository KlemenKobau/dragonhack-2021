package com.fribees.business.dto;

import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Schema(name = "User performance", description="POJO that wraps the current user performance")
public class UserPerformanceDTO {

    @Schema(description="Users performance as a double")
    private double userPerformance;

    public double getUserPerformance() {
        return userPerformance;
    }

    public void setUserPerformance(double userPerformance) {
        this.userPerformance = userPerformance;
    }
}
