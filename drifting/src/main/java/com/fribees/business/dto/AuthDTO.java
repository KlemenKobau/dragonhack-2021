package com.fribees.business.dto;

import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Schema(name="Authentication", description="Used for basic authentication")
public class AuthDTO {

    @Schema(description="User's username")
    private String username;

    @Schema(description="User's password")
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
