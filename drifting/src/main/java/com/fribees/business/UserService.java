package com.fribees.business;

import com.fribees.persistence.AppUser;

import javax.enterprise.context.RequestScoped;
import javax.transaction.Transactional;
import javax.ws.rs.ForbiddenException;
import java.util.List;
import java.util.Optional;

@RequestScoped
public class UserService {
    public List<AppUser> getAllUsers() {
        return AppUser.listAll();
    }

    public AppUser getUserById(long userId) {
        return AppUser.findById(userId);
    }

    @Transactional
    public void saveUsers(List<AppUser> appUsers) {
        AppUser.persist(appUsers);
    }

    public AppUser authenticate(String username, String password) {
        Optional<AppUser> authUserOpt = AppUser.getByUsernameAndPassword(username, password);

        if (authUserOpt.isEmpty()) {
            throw new ForbiddenException();
        }
        return authUserOpt.get();
    }
}
