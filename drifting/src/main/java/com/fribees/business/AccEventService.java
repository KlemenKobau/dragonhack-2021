package com.fribees.business;

import com.fribees.business.dto.ArduinoDTO;
import com.fribees.persistence.AccelerationEvent;
import com.fribees.persistence.Ride;

import javax.enterprise.context.RequestScoped;
import javax.transaction.Transactional;
import java.time.Instant;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Stream;

@RequestScoped
public class AccEventService {

    private static final Logger LOG = Logger.getLogger(AccEventService.class.getName());

    @Transactional
    public void saveEvents(List<AccelerationEvent> accelerationEvents) {
        AccelerationEvent.persist(accelerationEvents);
    }

    @Transactional
    public void saveArduinoEvents(List<ArduinoDTO> arduinoDTOS) {
        LOG.info(() -> String.format("Received DTOS: %s", arduinoDTOS));

        Instant currentTime = Instant.now();
        Ride ride = new Ride();

        ride.setRideEndTime(currentTime);
        ride.setRideStartTime(currentTime.minusSeconds(arduinoDTOS.get(0).getDriveDuration()));

        Stream<AccelerationEvent> eventStream = arduinoDTOS.stream()
                .map(arduinoDTO -> mapArduinoDTOsToAccelerationEvent(arduinoDTO, ride, currentTime));

        Ride.persist(ride);
        AccelerationEvent.persist(eventStream);
    }

    private AccelerationEvent mapArduinoDTOsToAccelerationEvent(ArduinoDTO arduinoDTO, Ride ride, Instant mappingTime) {
        AccelerationEvent accelerationEvent = new AccelerationEvent();
        accelerationEvent.setAngle(arduinoDTO.getAngle());
        accelerationEvent.setSize(arduinoDTO.getSize());
        accelerationEvent.setRide(ride);
        accelerationEvent.setTimeOfEvent(mappingTime.minusSeconds(arduinoDTO.getAmountPassedInSeconds()));

        ride.getAccelerationEvents().add(accelerationEvent);

        return accelerationEvent;
    }
}
