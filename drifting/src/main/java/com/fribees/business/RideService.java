package com.fribees.business;

import com.fribees.business.dto.AbsoluteEventsDTO;
import com.fribees.business.dto.Granularity;
import com.fribees.business.dto.StatisticsDTO;
import com.fribees.business.dto.UserPerformanceDTO;
import com.fribees.persistence.AccelerationEvent;
import com.fribees.persistence.AppUser;
import com.fribees.persistence.Ride;
import io.quarkus.panache.common.Page;
import io.quarkus.panache.common.Sort;

import javax.enterprise.context.RequestScoped;
import javax.transaction.Transactional;
import javax.ws.rs.BadRequestException;
import java.time.Duration;
import java.time.Instant;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

@RequestScoped
public class RideService {

    public List<Ride> getAllForUser(long userId) {
        return Ride.getForUser(userId);
    }

    public List<StatisticsDTO> getRideStatistics(long userId, Granularity granularity) {
        if (granularity == null) {
            throw new BadRequestException();
        }

        Instant endTime = Instant.now();
        switch (granularity) {
            case MONTHS:
                return getStatistics(userId, endTime.minus(Duration.ofDays(365)), endTime, Duration.ofDays(31));
            case DAYS:
                return getStatistics(userId, endTime.minus(Duration.ofDays(31)), endTime, Duration.ofDays(1));
            case ALL_TIME:
                return List.of(statisticsFromRides(Ride.listAll(), null, null));
            default:
                throw new BadRequestException();

        }
    }

    public AbsoluteEventsDTO getLastFiveRidesStatistics(long userId) {
        List<Ride> lastFiveRides = Ride.find("user.id", Sort.descending("rideStartTime"), userId)
                .page(Page.ofSize(5)).list();
        int numberOfEvents = lastFiveRides.stream().map(ride -> ride.getAccelerationEvents().size()).
                reduce(Integer::sum).orElse(0);

        AbsoluteEventsDTO absoluteEventsDTO = new AbsoluteEventsDTO();
        absoluteEventsDTO.setNumberOfEvents(numberOfEvents);

        return absoluteEventsDTO;
    }

    public UserPerformanceDTO getUserPerformance(long userId) {
        UserPerformanceDTO userPerformanceDTO = new UserPerformanceDTO();
        userPerformanceDTO.setUserPerformance(81.3);

        return userPerformanceDTO;
    }

    private List<StatisticsDTO> getStatistics(long userId, Instant startTime, Instant endTime, Duration granularity) {
        Instant currentEndTime = startTime.plus(granularity);
        Instant currentStartTime = startTime;

        List<StatisticsDTO> rideStatistics = new LinkedList<>();
        while (currentStartTime.isBefore(endTime)) {
            List<Ride> rides = Ride.getRidesInTimeFrame(startTime, currentEndTime, userId);
            rideStatistics.add(statisticsFromRides(rides, currentStartTime, currentEndTime));
            currentEndTime = currentEndTime.plus(granularity);
            currentStartTime = currentStartTime.plus(granularity);
        }

        return rideStatistics;
    }
    
    private StatisticsDTO statisticsFromRides(List<Ride> rides, Instant startTime, Instant endTime) {
        StatisticsDTO statisticsDTO = new StatisticsDTO();
        statisticsDTO.setStartTime(startTime);
        statisticsDTO.setEndTime(endTime);

        long totalDurationInMinutes = 0;
        long numberOfMoments = 0;
        long numberOfRides = rides.size();

        for (Ride ride : rides) {
            Duration rideDuration = Duration.between(ride.getRideStartTime(), ride.getRideEndTime());
            totalDurationInMinutes = totalDurationInMinutes + rideDuration.toMinutes();
            numberOfMoments = numberOfMoments + ride.getAccelerationEvents().size();
        }

        if (rides.size() == 0) {
            statisticsDTO.setAvgMomentsPerHour(0);
            statisticsDTO.setAvgMomentsPerRide(0);
        } else {
            statisticsDTO.setAvgMomentsPerHour( numberOfMoments * 60.0 / totalDurationInMinutes);
            statisticsDTO.setAvgMomentsPerRide( numberOfMoments * 60.0 / numberOfRides);
        }

        return statisticsDTO;
    }

    /**
     * A method for mocking the data
     */
    @Transactional
    public void saveMock() {
        Random random = new Random(42);
        AppUser mockUser = new AppUser();
        mockUser.setName("marjan");
        mockUser.setUsername("marjan");
        mockUser.setPassword("dragonhack");
        mockUser.setSurname("prehitri");

        Instant endTime = Instant.now();
        Instant startTime = endTime.minus(Duration.ofDays(365));
        Instant currentEndTime = startTime.plus(Duration.ofDays(1));

        List<Ride> rides = new LinkedList<>();
        List<AccelerationEvent> accelerationEvents = new LinkedList<>();

        while (startTime.isBefore(endTime)) {

            int numRides = random.nextInt(1) + 1;
            for (int i = 0; i < numRides; i++) {
                Ride ride = new Ride();
                ride.setUser(mockUser);
                mockUser.getRides().add(ride);
                rides.add(ride);

                int randOffset = random.nextInt(20) + 360 * i;

                ride.setRideStartTime(startTime.plus(Duration.ofMinutes(randOffset)));
                int rideDuration = random.nextInt(60) + 1;

                ride.setRideEndTime(ride.getRideStartTime().plus(Duration.ofMinutes(rideDuration)));

                int numRandEvents = random.nextInt(3) + 1;
                for (int j = 0; j < numRandEvents; j++) {
                    AccelerationEvent accelerationEvent = new AccelerationEvent();
                    accelerationEvent.setRide(ride);
                    accelerationEvent.setSize(random.nextDouble() * 10);
                    accelerationEvent.setAngle(random.nextDouble() * 1.4 - 0.7);
                    accelerationEvents.add(accelerationEvent);

                    int randEventOffset = random.nextInt(rideDuration) + 1;
                    accelerationEvent.setTimeOfEvent(ride.getRideStartTime().plus(Duration.ofMinutes(randEventOffset)));

                    accelerationEvent.setRide(ride);
                    ride.getAccelerationEvents().add(accelerationEvent);
                }

            }
            
            startTime = startTime.plus(Duration.ofDays(1));
            currentEndTime = currentEndTime.plus(Duration.ofDays(1));
        }

        AppUser.persist(mockUser);
        Ride.persist(rides);
        AccelerationEvent.persist(accelerationEvents);
    }
}
