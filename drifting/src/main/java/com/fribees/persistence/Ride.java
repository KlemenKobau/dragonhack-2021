package com.fribees.persistence;

import io.quarkus.hibernate.orm.panache.PanacheEntity;
import io.quarkus.panache.common.Sort;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.time.Instant;
import java.util.LinkedList;
import java.util.List;

@Entity
@Schema(description="Represents a single ride")
public class Ride extends PanacheEntity {

    @Schema(description="Ride's start time")
    private Instant rideStartTime;

    @Schema(description="Ride's end time")
    private Instant rideEndTime;

    @Schema(description="Arduino's internal ride id")
    private Long arduinoId;

    @ManyToOne
    @JsonbTransient
    private AppUser user;

    @OneToMany(mappedBy = "ride")
    @Schema(description="The acceleration events that happened during the ride")
    private List<AccelerationEvent> accelerationEvents;

    public static List<Ride> getForUser(long userId) {
        return list("user.id", Sort.by("rideStartTime", Sort.Direction.Descending), userId);
    }

    public static List<Ride> getRidesInTimeFrame(Instant startTime, Instant endTime, long userId) {
        return list("rideStartTime > ?1 and rideStartTime < ?2 and user.id = ?3", startTime, endTime, userId);
    }

    public Instant getRideStartTime() {
        return rideStartTime;
    }

    public void setRideStartTime(Instant rideStartTime) {
        this.rideStartTime = rideStartTime;
    }

    public Instant getRideEndTime() {
        return rideEndTime;
    }

    public void setRideEndTime(Instant rideEndTime) {
        this.rideEndTime = rideEndTime;
    }

    public List<AccelerationEvent> getAccelerationEvents() {
        if (accelerationEvents == null) {
            accelerationEvents = new LinkedList<>();
        }
        return accelerationEvents;
    }

    public void setAccelerationEvents(List<AccelerationEvent> accelerationEvents) {
        this.accelerationEvents = accelerationEvents;
    }

    public AppUser getUser() {
        return user;
    }

    public void setUser(AppUser user) {
        this.user = user;
    }

    public Long getArduinoId() {
        return arduinoId;
    }

    public void setArduinoId(Long arduinoId) {
        this.arduinoId = arduinoId;
    }
}
