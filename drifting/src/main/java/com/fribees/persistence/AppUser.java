package com.fribees.persistence;

import io.quarkus.hibernate.orm.panache.PanacheEntity;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Entity
@Schema(name="Application user", description="User entity class")
public class AppUser extends PanacheEntity {

    @Schema(description="User's username")
    private String username;

    @JsonbTransient
    private String password;

    @Schema(description="User's name")
    private String name;

    @Schema(description="User's surname")
    private String surname;

    @OneToMany(mappedBy = "user")
    @Schema(description="User's rides")
    private List<Ride> rides;

    public static Optional<AppUser> getByUsernameAndPassword(String username, String password) {
        return find("username = ?1 and password = ?2", username, password).firstResultOptional();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public List<Ride> getRides() {
        if (rides == null) {
            rides = new LinkedList<>();
        }
        return rides;
    }

    public void setRides(List<Ride> rides) {
        this.rides = rides;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
