package com.fribees.persistence;

import io.quarkus.hibernate.orm.panache.PanacheEntity;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.time.Instant;

@Entity
@Schema(name = "Acceleration event", description = "Entity class for the acceleration events")
public class AccelerationEvent extends PanacheEntity {

    @Schema(description = "Represents the time of the event")
    private Instant timeOfEvent;

    @Schema(description = "Represents the size of the acceleration vector")
    private Double size;

    @Schema(description = "Represents the angle of the acceleration vector")
    private Double angle;

    @ManyToOne
    @JsonbTransient
    private Ride ride;

    public Instant getTimeOfEvent() {
        return timeOfEvent;
    }

    public void setTimeOfEvent(Instant timeOfEvent) {
        this.timeOfEvent = timeOfEvent;
    }

    public Double getSize() {
        return size;
    }

    public void setSize(Double size) {
        this.size = size;
    }

    public Double getAngle() {
        return angle;
    }

    public void setAngle(Double angle) {
        this.angle = angle;
    }

    public Ride getRide() {
        return ride;
    }

    public void setRide(Ride ride) {
        this.ride = ride;
    }
}
