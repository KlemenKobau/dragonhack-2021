# Road rage
Not only is aggressive driving reckless and incredibly dangerous, it also significantly increases our carbon footprint. 
It leads to higher fuel consumption and thus contributes to global warming. 
Our solution is called RoadRage.  
It is a device that can be installed in a car that detects rapid acceleration and braking after which the driver is alerted with beeping. 

This repository consists of a vue frontend, a quarkus backend application
and the code for the arduino board.

The demo application is available at [DEMO](http://83.212.82.186/#/login) (username: marjan, password: dragonhack).

Directories:
- **vue frontend** -> folder named **road-rage**
- **quarkus backend** -> folder named **drifting**
- **arduino code** -> folder named **iot**

Each directory contains a README with additional information.

## Deployment
Requirements:
- docker
- docker-compose
- java 11

We have prepared a handy script for deploying **deploy.sh**.
Take care however that you change the container tags in the
**docker-compose.yml** to your username.

The images will automatically build and deploy to docker hub.

## Documentation
We have OpenAPI documentation available at [http://83.212.82.186:8080/q/swagger-ui](http://83.212.82.186:8080/q/swagger-ui/#/). 

## Video
Presentation of our project is available at [Youtube](https://www.youtube.com/watch?v=MaKJmCZKL6Y).
