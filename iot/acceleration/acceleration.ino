#include <Wire.h>
#include <Ticker.h>
#include <EEPROM.h>
#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <ESP8266WebServer.h>
#include <ArduinoJson.h>

#define I2C_ADDRESS 0b1101000
#define EVENTS_SIZE 100
#define MOVING_WINDOW_SIZE 20
#define LONG_TERM_AVERAGE_WINDOW_SIZE 1000
#define BUZZER_PIN D7
#define SERVER_PORT 8080
#define SERVER_HOST "http://83.212.82.186/"
#define SERVER_ENDPOINT "http://83.212.82.186:8080/acceleration/arduino"

struct AccelerationEvent {
  long millis;
  float size;
  float angle;
};

HTTPClient http;

Ticker accelerationTicker;

float rawAccelerationAverageLT[3];
int numberOfSamples = 0;
float accelerationMovingWindow[MOVING_WINDOW_SIZE][3];
int accelerationMovingWindowHead = 0;
long lastWarning = 0;
long lastAccelerationEvent = 0;
bool alert = false;
long driveId = random(0, 999999999);
long userId = 0;

int16_t lastAccReading[3];

AccelerationEvent eventsQueue[EVENTS_SIZE];
int eventsQueueHead = 0;
int eventsQueueSize = 0;



void setup() {
  // setup storage and comunication
  Serial.begin(115200);
  Wire.begin(12, 14);
  Wire.setClock(100000);

  // init buffers
  for (int i = 0; i < 3; i++) {
    rawAccelerationAverageLT[i] = 0;
  }
  for (int i = 0; i < MOVING_WINDOW_SIZE; i++) {
    for (int j = 0; j < 3; j++) {
      accelerationMovingWindow[i][j] = 0;
    }
  }
  accelerationTicker.attach_ms(100, measurement);

  pinMode(BUZZER_PIN, OUTPUT);

  Serial.println("EEPROM test");
  EEPROM.begin(512);
  userId = EEPROM.read(202);
  EEPROM.end();


  initialSetup();
}

void initialSetup() {
  EEPROM.begin(512);
  int8_t setupCompleted = EEPROM.read(201);
  EEPROM.end();
  if (setupCompleted != 1) {
  }
}

float convertAccelerationToMss(int16_t acceleration) {
  return acceleration / 1500.0;
}

void measurement() {
  readAcceleration();

  // computing long term average
  for (int i = 0; i < 3; i++) {
    rawAccelerationAverageLT[i] = (lastAccReading[i] + rawAccelerationAverageLT[i] * numberOfSamples) / (numberOfSamples + 1);
  }

  // filter acceleration values
  for (int i = 0; i < 3; i++) {
    lastAccReading[i] = lastAccReading[i] - rawAccelerationAverageLT[i];
  }

  // write to moving window
  for (int i = 0; i < 3; i++) {
    accelerationMovingWindow[accelerationMovingWindowHead][i] = convertAccelerationToMss(lastAccReading[i]);
  }
  accelerationMovingWindowHead = (accelerationMovingWindowHead + 1) % MOVING_WINDOW_SIZE;

  numberOfSamples = min(numberOfSamples + 1, LONG_TERM_AVERAGE_WINDOW_SIZE);

  detectHardAccelerations();
}

void detectHardAccelerations() {
  static float averageAccelerationX = 0.0;
  static float averageAccelerationY = 0.0;
  static float averageAccelerationZ = 0.0;

  // sum values from mooving window
  for (int i = 0; i < MOVING_WINDOW_SIZE; i++) {
    averageAccelerationX += accelerationMovingWindow[i][0];
    averageAccelerationY += accelerationMovingWindow[i][1];
    averageAccelerationZ += accelerationMovingWindow[i][2];
  }
  averageAccelerationX /= MOVING_WINDOW_SIZE;
  averageAccelerationY /= MOVING_WINDOW_SIZE;
  averageAccelerationZ /= MOVING_WINDOW_SIZE;

  float accelerationAmplitude = sqrt(averageAccelerationX * averageAccelerationX + 
                                     averageAccelerationY * averageAccelerationY + 
                                     averageAccelerationZ * averageAccelerationZ);

  if (abs(averageAccelerationY) > 0) {
    float accelerationDirection = atan(averageAccelerationX / averageAccelerationY);

    if (abs(accelerationDirection) < 0.79 && accelerationAmplitude > 3) {
      soundAlert();
    }
    if (abs(accelerationDirection) < 0.79 && accelerationAmplitude > 2) {
      createAccelerationEvent(accelerationAmplitude, accelerationDirection);
    }
  }
}

void createAccelerationEvent(float size, float angle) {
  if (millis() - lastAccelerationEvent > 5000) {
    eventsQueue[eventsQueueHead].millis = millis();
    eventsQueue[eventsQueueHead].size = size;
    eventsQueue[eventsQueueHead].angle = angle;
    eventsQueueHead = (eventsQueueHead + 1) % EVENTS_SIZE;
    eventsQueueSize++;
    Serial.println(eventsQueueSize);
  }
  lastAccelerationEvent = millis();
}

void soundAlert() {
  if (millis() - lastWarning > 10000) {
    Serial.println("Sound alert triggered");
    lastWarning = millis();
    alert = true;
  }
}

void readAcceleration() {
  int BYTE_NUM = 6;
  Wire.beginTransmission(I2C_ADDRESS);
  Wire.write(59);
  Wire.endTransmission();
  
  Wire.requestFrom(I2C_ADDRESS, BYTE_NUM);
  for (int q = 0; q < BYTE_NUM; q++) {
    uint16_t byteValue = Wire.read();
    lastAccReading[q / 2] &= 0xFF << (8 * (q % 2));
    lastAccReading[q / 2] |= byteValue << (8 * (1 - q % 2));
  }
}

void sendAccelerationDataToServer() {
  if (WiFi.status() != WL_CONNECTED) {
    EEPROM.begin(512);
    int8_t setupCompleted = EEPROM.read(201);
    if (setupCompleted != 1) {
      return;
    }
    WiFi.mode(WIFI_STA);
    WiFi.begin(readStringFromEEPROM(1), readStringFromEEPROM(101));
    EEPROM.end();
    Serial.print("Connecting to WiFi");
    int waitingDuration = 0;
    while (WiFi.status() != WL_CONNECTED){
        delay(500);
        Serial.print(".");
        if (waitingDuration++ > 20) {
          // wifi not available
          Serial.print("WiFi not available");
          return;
        }
    }
    Serial.print("IP address = ");
    Serial.println(WiFi.localIP());
  }

  for (int i = 0; i < eventsQueueSize; i++) {
    Serial.print("Sending event ");
    Serial.println(i);
    Serial.println(eventsQueue[(EVENTS_SIZE + eventsQueueHead - i - 1) % EVENTS_SIZE].size);
    Serial.println(eventsQueue[(EVENTS_SIZE + eventsQueueHead - i - 1) % EVENTS_SIZE].angle);
    StaticJsonDocument<200> json;
    json["userId"] = userId;
    json["driveDuration"] = millis() / 1000;
    json["driveId"] = driveId;
    json["size"] = eventsQueue[(EVENTS_SIZE + eventsQueueHead - i - 1) % EVENTS_SIZE].size;
    json["angle"] = eventsQueue[(EVENTS_SIZE + eventsQueueHead - i - 1) % EVENTS_SIZE].angle;
    json["amountPassedInSeconds"] = (millis() - eventsQueue[(EVENTS_SIZE + eventsQueueHead - i - 1) % EVENTS_SIZE].millis) / 1000;
    String outputJSON;
    serializeJsonPretty(json, outputJSON);
    Serial.println(outputJSON);
    WiFiClient client;
    client.connect(SERVER_HOST, SERVER_PORT);
    http.begin(client, SERVER_ENDPOINT);
    http.addHeader("Content-Type", "application/json");
    int httpCode = http.POST("[" + outputJSON + "]");
    Serial.println(httpCode);
    http.end();
  }
  eventsQueueSize = 0;
}

void writeStringToEEPROM(int addrOffset, const String &strToWrite) {
  byte len = strToWrite.length();
  EEPROM.write(addrOffset, len);
  for (int i = 0; i < len; i++) {
    EEPROM.write(addrOffset + 1 + i, strToWrite[i]);
  }
}

String readStringFromEEPROM(int addrOffset) {
  int newStrLen = EEPROM.read(addrOffset);
  char data[newStrLen + 1];
  for (int i = 0; i < newStrLen; i++) {
    data[i] = EEPROM.read(addrOffset + 1 + i);
  }
  data[newStrLen] = '\0';
  return String(data);
}

long idx = 0;
void loop() {
  if (alert) {
    alert = false;
    for (int i = 0; i < 2; i++) {
      digitalWrite(BUZZER_PIN, HIGH);
      delay(100);
      digitalWrite(BUZZER_PIN, LOW);
      delay(100);
    }
  }
  if (idx % 100 == 0) {
    sendAccelerationDataToServer();
  }
  delay(1000);
  idx++;
}