cd drifting || exit
echo 'Building backend'
./mvnw clean package -Dquarkus.container-image.build=true
cd ..

docker-compose build frontend
docker-compose push
