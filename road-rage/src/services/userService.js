import axios from "axios";

export default class UserService {
    getCurrentUser(username, password) {
        return axios.post(process.env.VUE_APP_BACKEND_URL + '/users/authenticate', {username: username, password: password})
    }
}
