import axios from "axios";

export default class RideService {

    getRidesForCurrentUser(userId) {
        return axios.get(process.env.VUE_APP_BACKEND_URL + '/rides/' + userId)
            .catch(reason => console.log(reason));
    }

    getAllTimeAverageRides(userId) {
        return axios.get(process.env.VUE_APP_BACKEND_URL + '/rides/' + userId + '/statistics/?granularity=ALL_TIME')
            .catch(reason => console.log(reason));
    }

    getEventInLastFiveRides(userId) {
        return axios.get(process.env.VUE_APP_BACKEND_URL + '/rides/' + userId + '/statistics/five-rides')
            .catch(reason => console.log(reason));
    }

    getUserPerformance(userId) {
        return axios.get(process.env.VUE_APP_BACKEND_URL + '/rides/' + userId + '/statistics/performance')
            .catch(reason => console.log(reason));
    }


    getRidesStatisticsCurrentUser(userId, granularity) {
        return axios.get(process.env.VUE_APP_BACKEND_URL + '/rides/' + userId + '/statistics?granularity=' + granularity)
            .catch(reason => console.log(reason));
    }
}
